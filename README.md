# TCC2: O início do fim?


<div align="center">
<p>
Uma doce existência pífia 😎
</p>
</div>

<div align="center">
<img src="imagens/solitude.png" alt="Novos Horizontes">
</div>

<div align="center">
<p>
<b>Fonte:</b> Imagem gerada por [Cayron](https://www.craiyon.com/)
</p>
</div>

## O que é esse projeto?

* O projeto em questão está sendo desenvolvido para o Trabalho de Conclusão de Curso. Trata-se de um projeto que tem como objetivo determinar os parâmetros de ondas em ventos atmosféricos da região MLT.

## Como funciona?
* O funcionamento se dá por meio de algumas tecnicas de analise de espectro e analise harmônica.

## Com interesse no projeto?

* Utilizando um terminal, crie primeiro um ambiente virtual do python dentro de uma pasta de interesse.
``` 
python -m venv .v
```

* Feito isso, ainda utilizando o termina, você pode baixar o projeto utilizando o git por meio das seguinte linhas de comando.

```
git clone https://gitlab.com/lj912290/tcc2.git

```

## Como resolver as dependências? 

* rode o seguinte comando.

``` 
pip install requiriments.txt
```
* Esse comando irá resolver automaticamente as dependêndias do projeto.

## Quer contribuir em algo?

Email para contato
- lj912290@gmail.com